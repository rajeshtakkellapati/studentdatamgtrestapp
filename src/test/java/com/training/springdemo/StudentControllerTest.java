package com.training.springdemo;

import com.training.springdemo.controller.StudentController;
import com.training.springdemo.model.Student;
import com.training.springdemo.service.StudentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@RunWith(SpringRunner.class)
@WebMvcTest(value = StudentController.class, excludeAutoConfiguration = SecurityAutoConfiguration.class)
public class StudentControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private StudentService studentService;

    @Test
    public void getAllStudentsTest() throws Exception {
        Student raj = new Student("Rajesh", "rajesh@techera.org", 9550123456L);
        Student ven = new Student("Venky", "venky@techera.org", 9550123456L);
        Student pav = new Student("Pavan", "pavan@techera.org", 9550123456L);

        List<Student> studentList = Arrays.asList(raj, ven, pav);

        given(studentService.getStudentData()).willReturn(studentList);

        mvc.perform(get("/api/students")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].name", is(raj.getName())))
                .andExpect(jsonPath("$[1].name", is(ven.getName())))
                .andExpect(jsonPath("$[2].name", is(pav.getName())));

        verify(studentService, VerificationModeFactory.times(1)).getStudentData();

        reset(studentService);
    }

    @Test
    public void createStudentTest() throws Exception {
        Student raj = new Student("Rajesh", "rajesh@techera.org", 9550123456L);

        given(studentService.addStudent(Mockito.any())).willReturn(raj);

        mvc.perform(post("/api/students")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJson(raj)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name",is("Rajesh")));

        verify(studentService, VerificationModeFactory.times(1)).addStudent(Mockito.any());

        reset(studentService);
    }
}
