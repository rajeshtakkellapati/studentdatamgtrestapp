package com.training.springdemo;

import com.training.springdemo.dao.StudentDAO;
import com.training.springdemo.model.Student;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class StudentDAOTest {

    @Autowired
    @Qualifier(value = "com.training.springdemo.dao.StudentDAOImpl")
    private StudentDAO studentDAO;

    @Test
    public void getAllEmployeesTest() {


        List<Student> studentList = studentDAO.findAll();

        assertThat(studentList).hasSize(5);
    }
}
