package com.training.springdemo.dao;

import com.training.springdemo.model.Student;

import java.util.List;

public interface StudentDAO {
    public List<Student> findAll();

    public Student saveStudent(Student student);

    public void removeStudent(long id);

    public Student getStudent(long id);
}
