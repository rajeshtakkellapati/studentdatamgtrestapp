package com.training.springdemo.model;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class StudentData {
    private static long idValue = 1;
    private final List<Student> studentList = new ArrayList<>();

    public StudentData() {
        addStudent(new Student("Rajesh", "rajesh@techera.org", 9550123456L));
        addStudent(new Student("Venky", "venky@techera.org", 9550123457L));
        addStudent(new Student("Pavan", "pavan@techera.org", 9550123458L));
        addStudent(new Student("Shiva", "shiva@techera.org",9550123459L));
    }

    public void addStudent(Student student) {
        student.setId(idValue);
        studentList.add(student);
        idValue++;
    }

    public void removeStudent(long id) {
        ListIterator<Student> studentListIterator = studentList.listIterator();

        while (studentListIterator.hasNext()) {
            Student student = studentListIterator.next();

            if(student.getId() == id) {
                studentListIterator.remove();
                break;
            }
        }
    }

    public Student getStudent(long id) {
        for(Student student: studentList) {
            if(student.getId() == id) {
                return student;
            }
        }
        return null;
    }

    public void updateStudent(Student student) {
        ListIterator<Student> studentListIterator = studentList.listIterator();

        while (studentListIterator.hasNext()) {
            Student myStudent = studentListIterator.next();

            if(myStudent.equals(student)) {
                studentListIterator.set(student);
                break;
            }
        }
    }

    public List<Student> getStudentList() {
        return studentList;
    }
}
