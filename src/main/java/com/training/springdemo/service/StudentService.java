package com.training.springdemo.service;

import com.training.springdemo.model.Student;

import java.util.List;

public interface StudentService {
    Student addStudent(Student student);

    void removeStudent(long id);

    Student getStudent(long id);

    Student updateStudent(Student student);

    List<Student> getStudentData();
}
