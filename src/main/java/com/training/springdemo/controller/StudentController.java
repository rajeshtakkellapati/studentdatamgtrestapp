package com.training.springdemo.controller;

import com.training.springdemo.model.Student;
import com.training.springdemo.model.StudentNotFoundException;
import com.training.springdemo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/")
public class StudentController {

    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("students")
    public ResponseEntity<List<Student>> getAllStudents() {
        return new ResponseEntity<>(studentService.getStudentData(), HttpStatus.OK);
    }

    @PostMapping("students")
    public ResponseEntity<Student> addStudent(@RequestBody Student student){
        studentService.addStudent(student);
        return new ResponseEntity<>(student, HttpStatus.CREATED);
    }


    @GetMapping("students/{id}")
    public ResponseEntity<Student> getStudentById(@PathVariable("id") long id) {
        Student student = studentService.getStudent(id);

        if(student == null) {
            throw new StudentNotFoundException("Student id not found - " + id);
        }

        return new ResponseEntity<>(student, HttpStatus.OK);
    }

    @PutMapping("students")
    public ResponseEntity<Student> updateStudent(@RequestBody Student student) {
        studentService.updateStudent(student);
        return new ResponseEntity<>(student, HttpStatus.OK);
    }

    @DeleteMapping("students/{id}")
    public ResponseEntity<String> deleteStudent(@PathVariable("id") long id){
        studentService.removeStudent(id);
        return new ResponseEntity<>("Student with id " + id + " deleted.", HttpStatus.OK);
    }
}
